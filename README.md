# Arduino Library for control panel with I2C PCF8574 adapter, Liquid Crystal display and keys

based on [https://github.com/mathertel/LiquidCrystal_PCF8574](https://github.com/mathertel/LiquidCrystal_PCF8574)

A library & schematic for using a HD44870 LCD and a set of keys from one PCF8574. Hopefully can save you a few wires when building a control panel.

### Theory of operation

This library uses the idle time between LCD writes to read the state of up to six keys. 

After writing to the LCD, the Enable (EN/P2) is set low and the remaining PCF IO, apart from the Backlight control pin, is set HIGH (internal pullup).

The keys connect this low EN signal to their respective data pins, pulling them low and allowing the change to be read over i2c.

Diodes prevent keypresses from corrupting the data lines while the LCD is active.

### Schematic

Clearly there's some stuff missing here like the contrast control etc, but it's just to give you an idea of how the keys are hooked up.

![schematic](./images/sch.png)

These are the same connections as many generic "LCD backpacks" such as this one:

![backpack](./images/backpack.jpg)

This same technique might work on other backpacks, but the key lines have to be pulled up. In this case this is done by the internal PCF pullups.

### How to use

`#include <LCD_Control_Panel.h>` in sketch.

`lcd.readKeys()` returns a uint8_t with the keys bitmap. In the default pin setup, bits 2 and 3 should be ignored - these are the EN and BACKLIGHT pins respectively. Since the PCF inputs are pulled up, the logic is inverted (a 1 bit = key not pressed).

See the `keys` example for details.

