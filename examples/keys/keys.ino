// This example shows reading keys and writing to the LCD.

#include <Arduino.h>
#include <Wire.h>
#include <LCD_Control_Panel.h>

LCD_Control_Panel lcd(0x27);  // set the LCD address to 0x27 for a 16 chars and 2 line display

uint32_t t=0;
uint32_t keyTimer=0;

// 2 custom characters

byte dotOff[] = { 0b00000, 0b01110, 0b10001, 0b10001,
                  0b10001, 0b01110, 0b00000, 0b00000 };
byte dotOn[] = { 0b00000, 0b01110, 0b11111, 0b11111,
                 0b11111, 0b01110, 0b00000, 0b00000 };

void setup() {
  int error;

  Serial.begin(115200);
  Serial.println("LCD...");

  // wait on Serial to be available on Leonardo
  while (!Serial)
    ;

  Serial.println("Probing for PCF8574 on address 0x27...");

  // See http://playground.arduino.cc/Main/I2cScanner how to test for a I2C device.
  Wire.begin();
  Wire.beginTransmission(0x27);
  error = Wire.endTransmission();
  Serial.print("Error: ");
  Serial.print(error);

  if (error == 0) {
    Serial.println(": LCD found.");

    lcd.begin(16, 2);  // initialize the lcd

    lcd.createChar(1, dotOff);
    lcd.createChar(2, dotOn);

    lcd.setBacklight(255);

  } else {
    Serial.println(": LCD not found.");
  }  // if

}  // setup()


void loop() {
  if (millis() - t > 1000) {
    t=millis();
    lcd.home();
    lcd.printf ("time = %i",millis());
  }

  // Every ten ms, read the keys and print the results
  if (millis() - keyTimer > 10) {
    keyTimer = millis();
    lcd.setCursor (0,1);
    uint8_t k = lcd.readKeys();
    for (int i=0; i<8; i++) {
      lcd.print(k & 0x80 ? "\01" : "\02");
      k <<=1;
    }
  }
}  // loop()